# App: Support #

Please email 1to1software@gmail.com

# App: Privacy policy #

This app does not collect any personally identifiable information. All data (for example, app preferences, alarms, and any data entered by you) is stored on your device only, and can be simply erased by clearing the app's data or uninstalling it. 
No data that you enter is sent outside your device at any time.

If you find any security vulnerability that has been inadvertently caused, or have any question regarding how the app manages your information, please send an email with your concerns.

Yours sincerely,  
Brian Cole
Principal Engineer
1to1software@gmail.com
